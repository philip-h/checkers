/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author philipjnr.habib
 */
public class Checkers extends JFrame implements ActionListener
{
    private static final long serialVersionUID = 1L;
    private final JButton[][] chips = new JButton[8][8];
    private JButton reset, save, load;
    private final JLabel status = new JLabel();
    private ImageIcon redPiece, bluePiece, redKing, blueKing;
    private boolean turn = true;
    private boolean isFirstClick = true;
    private final Color boardColour = new Color(153, 102, 0);
    private int[] rowAndColOfPiece;

    public Checkers()
    {
        super("Philip's Awesome Checkers Game!");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    private void init()
    {
        bluePiece = new ImageIcon(Checkers.class.getResource("/images/blue.jpg"));
        redPiece = new ImageIcon(Checkers.class.getResource("/images/red.jpg"));
        blueKing = new ImageIcon(Checkers.class.getResource("/images/blueKing.jpg"));
        redKing = new ImageIcon(Checkers.class.getResource("/images/redKing.jpg"));

        JPanel mainPane = new JPanel(new BorderLayout());
        JPanel gridPane = new JPanel(new GridLayout(8, 8, 5, 5));
        gridPane.setBackground(Color.black);
        for (int row = 0; row < chips.length; row++)
        {
            for (int col = 0; col < chips[row].length; col++)
            {
                chips[row][col] = new JButton();
                chips[row][col].setPreferredSize(new Dimension(50, 49));
                chips[row][col].setBackground(boardColour);
                chips[row][col].setBorder(null);
                chips[row][col].addActionListener(this);

                gridPane.add(chips[row][col]);
            }
        }

        mainPane.add(gridPane, BorderLayout.CENTER);
        status.setHorizontalAlignment(0);
        mainPane.add(status, BorderLayout.NORTH);

        JPanel buttonPane = new JPanel(new FlowLayout());
        reset = new JButton("Reset");
        save = new JButton("Save");
        save.setToolTipText("Save current game");
        load = new JButton("Load");
        load.setToolTipText("Load previous game");

        reset.addActionListener(this);
        save.addActionListener(this);
        load.addActionListener(this);
        
        buttonPane.add(reset);
        buttonPane.add(save);
        buttonPane.add(load);
        buttonPane.setBorder(BorderFactory.createEtchedBorder());

        mainPane.add(buttonPane, BorderLayout.SOUTH);
        setContentPane(mainPane);
        setBoard();
    }

    private void setBoard()
    {
        for (int i = 0; i < chips.length; i++)
        {
            for (int j = 0; j < chips[i].length; j += 2)
            {
                if (i < 3)
                {
                    if (i % 2 == 0)
                    {
                        chips[i][j + 1].setIcon(redPiece);
                    } else
                    {
                        chips[i][j].setIcon(redPiece);
                    }

                } else if (i > 4)
                {
                    if (i % 2 == 1)
                    {
                        chips[i][j].setIcon(bluePiece);
                    } else
                    {
                        chips[i][j + 1].setIcon(bluePiece);
                    }
                }

            }
        }

        status.setForeground(Color.blue);
        status.setText("Blue's Turn");
    }

    private void reset()
    {
        this.dispose();
        new Checkers();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {

        JButton buttonPressed = (JButton) e.getSource();

        if (buttonPressed == reset)
        {
            reset();
        } else if (buttonPressed == save)
        {
            saveCurrentGame();
        } else if (buttonPressed == load)
        {
            loadGame();
        } else
        {
            if (isFirstClick)
            {
                if (turn)
                {
                    if (buttonPressed.getIcon() == bluePiece)
                    {
                        highlightOptionsForBluePiece(buttonPressed);
                    } else if (buttonPressed.getIcon() == blueKing)
                    {
                        highlightOptionsForBlueKing(buttonPressed);
                    }
                } else
                {
                    if (buttonPressed.getIcon() == redPiece)
                    {
                        highlightOptionsForRedPiece(buttonPressed);
                    } else if (buttonPressed.getIcon() == redKing)
                    {
                        highlightOptionsForRedKing(buttonPressed);
                    }
                }
            } else
            {
                if (turn)
                {
                    if (buttonPressed.getBackground() == Color.cyan)
                    {
                        moveBluePiece(buttonPressed);
                        checkWin();
                    } else if (buttonPressed == chips[rowAndColOfPiece[0]][rowAndColOfPiece[1]])
                    {
                        resetHighlights();
                        isFirstClick = true;
                    }
                } else
                {
                    if (buttonPressed.getBackground() == Color.cyan)
                    {
                        moveRedPiece(buttonPressed);
                        checkWin();
                    } else if (buttonPressed == chips[rowAndColOfPiece[0]][rowAndColOfPiece[1]])
                    {
                        resetHighlights();
                        isFirstClick = true;
                    }
                }
            }
        }
    }

    private void highlightOptionsForBluePiece(JButton buttonPressed)
    {
        rowAndColOfPiece = getRowAndColOfPressedButton(buttonPressed);
        int row = rowAndColOfPiece[0];
        int col = rowAndColOfPiece[1];

        tryHighlightTopLeft(row, col, redPiece);
        tryHighlightTopRight(row, col, redPiece);

    }

    private void highlightOptionsForBlueKing(JButton buttonPressed)
    {
        rowAndColOfPiece = getRowAndColOfPressedButton(buttonPressed);
        int row = rowAndColOfPiece[0];
        int col = rowAndColOfPiece[1];

        tryHighlightTopLeft(row, col, redPiece);
        tryHighlightTopRight(row, col, redPiece);
        tryHighlightBottomLeft(row, col, redPiece);
        tryHighlightBottomRight(row, col, redPiece);
    }

    private void highlightOptionsForRedPiece(JButton buttonPressed)
    {
        rowAndColOfPiece = getRowAndColOfPressedButton(buttonPressed);
        int row = rowAndColOfPiece[0];
        int col = rowAndColOfPiece[1];

        tryHighlightBottomLeft(row, col, bluePiece);
        tryHighlightBottomRight(row, col, bluePiece);
    }

    private void highlightOptionsForRedKing(JButton buttonPressed)
    {
        rowAndColOfPiece = getRowAndColOfPressedButton(buttonPressed);
        int row = rowAndColOfPiece[0];
        int col = rowAndColOfPiece[1];

        tryHighlightTopLeft(row, col, bluePiece);
        tryHighlightTopRight(row, col, bluePiece);
        tryHighlightBottomLeft(row, col, bluePiece);
        tryHighlightBottomRight(row, col, bluePiece);
    }

    private void tryHighlightTopLeft(int row, int col, ImageIcon pieceToJump)
    {
        try
        {
            if (chips[row - 1][col - 1].getIcon() == null)
            {
                chips[row - 1][col - 1].setBackground(Color.cyan);
                isFirstClick = false;
            } else if (chips[row - 1][col - 1].getIcon() == pieceToJump)
            {
                if (chips[row - 2][col - 2].getIcon() == null)
                {
                    chips[row - 2][col - 2].setBackground(Color.cyan);
                    isFirstClick = false;
                }
            }
        } catch (ArrayIndexOutOfBoundsException aioobe)
        {
        }
    }

    private void tryHighlightTopRight(int row, int col, ImageIcon pieceToJump)
    {
        try
        {
            if (chips[row - 1][col + 1].getIcon() == null)
            {
                chips[row - 1][col + 1].setBackground(Color.cyan);
                isFirstClick = false;
            } else if (chips[row - 1][col + 1].getIcon() == pieceToJump)
            {
                if (chips[row - 2][col + 2].getIcon() == null)
                {
                    chips[row - 2][col + 2].setBackground(Color.cyan);
                    isFirstClick = false;
                }
            }
        } catch (ArrayIndexOutOfBoundsException aioobe)
        {
        }
    }

    private void tryHighlightBottomLeft(int row, int col, ImageIcon pieceToJump)
    {
        try
        {
            if (chips[row + 1][col - 1].getIcon() == null)
            {
                chips[row + 1][col - 1].setBackground(Color.cyan);
                isFirstClick = false;
            } else if (chips[row + 1][col - 1].getIcon() == pieceToJump)
            {
                if (chips[row + 2][col - 2].getIcon() == null)
                {
                    chips[row + 2][col - 2].setBackground(Color.cyan);
                    isFirstClick = false;
                }
            }
        } catch (ArrayIndexOutOfBoundsException aioobe)
        {
        }
    }

    private void tryHighlightBottomRight(int row, int col, ImageIcon pieceToJump)
    {
        try
        {
            if (chips[row + 1][col + 1].getIcon() == null)
            {
                chips[row + 1][col + 1].setBackground(Color.cyan);
                isFirstClick = false;
            } else if (chips[row + 1][col + 1].getIcon() == pieceToJump)
            {
                if (chips[row + 2][col + 2].getIcon() == null)
                {
                    chips[row + 2][col + 2].setBackground(Color.cyan);
                    isFirstClick = false;
                }
            }
        } catch (ArrayIndexOutOfBoundsException aioobe)
        {
        }
    }

    private void moveBluePiece(JButton buttonPressed)
    {
        JButton lastButtonClicked = chips[rowAndColOfPiece[0]][rowAndColOfPiece[1]];
        if (lastButtonClicked.getIcon() == bluePiece)
        {
            buttonPressed.setIcon(bluePiece);
        } else if (lastButtonClicked.getIcon() == blueKing)
        {
            buttonPressed.setIcon(blueKing);
        }

        lastButtonClicked.setIcon(null);
        int[] rowAndColOfMove = getRowAndColOfPressedButton(buttonPressed);
        if (Math.abs(rowAndColOfMove[0] - rowAndColOfPiece[0]) == 2)
        {
            int rowOfEatenPiece = (rowAndColOfMove[0] + rowAndColOfPiece[0]) / 2;
            int colOfEatenPiece = (rowAndColOfMove[1] + rowAndColOfPiece[1]) / 2;
            chips[rowOfEatenPiece][colOfEatenPiece].setIcon(null);
        }

        if (rowAndColOfMove[0] == 0)
        {
            buttonPressed.setIcon(blueKing);
        }
        resetHighlights();
        isFirstClick = true;
        turn = !turn;
        if (!turn)
        {
            status.setForeground(Color.red);
            status.setText("Red's Turn");
        }
    }

    private void moveRedPiece(JButton buttonPressed)
    {
        JButton lastButtonClicked = chips[rowAndColOfPiece[0]][rowAndColOfPiece[1]];
        if (lastButtonClicked.getIcon() == redPiece)
        {
            buttonPressed.setIcon(redPiece);
        } else if (lastButtonClicked.getIcon() == redKing)
        {
            buttonPressed.setIcon(redKing);
        }

        lastButtonClicked.setIcon(null);
        int[] rowAndColOfMove = getRowAndColOfPressedButton(buttonPressed);
        if (Math.abs(rowAndColOfMove[0] - rowAndColOfPiece[0]) == 2)
        {
            int rowOfEatenPiece = (rowAndColOfMove[0] + rowAndColOfPiece[0]) / 2;
            int colOfEatenPiece = (rowAndColOfMove[1] + rowAndColOfPiece[1]) / 2;
            chips[rowOfEatenPiece][colOfEatenPiece].setIcon(null);
        }

        if (rowAndColOfMove[0] == 7)
        {
            buttonPressed.setIcon(redKing);
        }
        resetHighlights();
        isFirstClick = true;
        turn = !turn;
        if (turn)
        {
            status.setForeground(Color.blue);
            status.setText("Blue's Turn");
        }
    }

    private int[] getRowAndColOfPressedButton(JButton buttonPressed)
    {
        int rowAndCol[] = new int[2];
        for (int i = 0; i < chips.length; i++)
        {
            for (int j = 0; j < chips[i].length; j++)
            {
                if (buttonPressed == chips[i][j])
                {
                    rowAndCol[0] = i;
                    rowAndCol[1] = j;
                    return rowAndCol;
                }
            }
        }
        return new int[]
        {
            -1, -1
        };
    }

    private void resetHighlights()
    {
        for (int i = 0; i < chips.length; i++)
        {
            for (int j = 0; j < chips[i].length; j++)
            {
                chips[i][j].setBackground(boardColour);
            }
        }
    }

    private void checkWin()
    {
        int numOfReds = 0, numOfBlues = 0;
        for (int i = 0; i < chips.length; i++)
        {
            for (int j = 0; j < chips[i].length; j++)
            {
                if (chips[i][j].getIcon() == redPiece || chips[i][j].getIcon() == redKing)
                {
                    numOfReds++;
                } else if (chips[i][j].getIcon() == bluePiece || chips[i][j].getIcon() == blueKing)
                {
                    numOfBlues++;
                }
            }
        }
        if (numOfBlues == 0)
        {
            JOptionPane.showMessageDialog(null, "Red Wins!!");
            reset();
        } else if (numOfReds == 0)
        {
            JOptionPane.showMessageDialog(null, "Blue Wins!!");
            reset();
        }

    }

    private void saveCurrentGame()
    {
        PrintWriter output = null;
        try
        {
            output = new PrintWriter(new FileWriter("savedGame.save"));
        } catch (IOException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to create new file writer: " + ex.getMessage());
        }
        if (output != null)
        {
            for (int i = 0; i < chips.length; i++)
            {
                for (int j = 0; j < chips[i].length; j++)
                {
                    ImageIcon currChip = (ImageIcon) chips[i][j].getIcon();
                    if (currChip == bluePiece)
                    {
                        output.print("b");
                    } else if (currChip == blueKing)
                    {
                        output.print("B");
                    } else if (currChip == redPiece)
                    {
                        output.print("r");
                    } else if (currChip == redKing)
                    {
                        output.print("R");
                    } else
                    {
                        output.print("x");
                    }
                }
                output.println();
            }
            output.close();
            JOptionPane.showMessageDialog(null, "Success","The game has been saved!", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void loadGame()
    {
        Scanner input = null;
        try
        {
            input = new Scanner(new FileReader("savedGame.save"));
        } catch (FileNotFoundException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to laod save file. Please make sure you save a game before loading it");
        }
        if (input != null)
        {
            for (int i = 0; i < chips.length; i++)
            {
                String row = input.nextLine();
                for (int j = 0; j < chips[i].length; j++)
                {
                    if (row.charAt(j) == 'b')
                    {
                        chips[i][j].setIcon(bluePiece);
                    } else if (row.charAt(j) == 'B')
                    {
                        chips[i][j].setIcon(blueKing);
                    } else if (row.charAt(j) == 'r')
                    {
                        chips[i][j].setIcon(redPiece);
                    } else if (row.charAt(j) == 'R')
                    {
                        chips[i][j].setIcon(redKing);
                    } else if (row.charAt(j) == 'x')
                    {
                        chips[i][j].setIcon(null);
                    }
                }
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        new Checkers();
    }

}
