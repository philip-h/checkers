package checkers;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author philipjnr.habib
 */
public class BoardPieces extends JButton
{
    private static final long serialVersionUID = 1L;
    private int row, column;
    private ImageIcon bluePiece, blueKing, redPiece, redKing;

    public BoardPieces(int row, int column)
    {
        super();
        this.row = row;
        this.column = column;
        setPreferredSize(new Dimension(50, 49));
        setBoardBackground();
        setBorder(null);
    }

    public int getRow()
    {
        return row;
    }

    public int getColumn()
    {
        return column;
    }

    public void setAsBluePiece()
    {
        super.setIcon(bluePiece);
    }
    
    public void setAsRedPiece()
    {
        super.setIcon(redPiece);
    }

    public void setAsKing()
    {
        if (hasBluePiece() || hasBlueKing())
            super.setIcon(blueKing);
        else if (hasRedPiece() || hasRedKing())
            super.setIcon(redKing);
    }
    
    public void setAsEmpty()
    {
        super.setIcon(null);
    }

    public void highlightSquare()
    {
        super.setBackground(Color.cyan);
    }
    
    public final void setBoardBackground()
    {
        super.setBackground(new Color(153, 102, 0));
    }

    public boolean hasBluePiece()
    {
        return super.getIcon() == bluePiece;
    }

    public boolean hasBlueKing()
    {
        return super.getIcon() == blueKing;
    }

    public boolean hasRedPiece()
    {
        return super.getIcon() == redPiece;
    }

    public boolean hasRedKing()
    {
        return super.getIcon() == redKing;
    }

    public boolean isEmpty()
    {
        return super.getIcon() == null;
    }
    
    public boolean isHighlighted()
    {
        return super.getBackground() == Color.cyan;
    }

    public boolean isEnemyPiece(BoardPieces potentialEnemy)
    {
        if (hasRedKing())
        {
            return potentialEnemy.hasBluePiece() || potentialEnemy.hasBlueKing();
        } else if (hasRedPiece())
        {
            return potentialEnemy.hasBluePiece();
        } else if (hasBlueKing())
        {
            return potentialEnemy.hasRedPiece() || potentialEnemy.hasRedKing();
        } else if (hasBluePiece())
        {
            return potentialEnemy.hasRedPiece();
        } else
        {
            return false;
        }
    }

}
